﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MBTA.Interfaces;

namespace MBTA.Services
{
    public class ScheduleHostedService: HostedService
    {
        private readonly ITrainScheduleProvider _scheduleProvider;
        private readonly IConfigurationWrapper _configurationWrapper;
        public ScheduleHostedService(ITrainScheduleProvider scheduleProvider, IConfigurationWrapper configurationWrapper)
        {
            this._scheduleProvider = scheduleProvider;
            _configurationWrapper = configurationWrapper;
        }

        public override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var dataRefreshIntervalInSeconds = int.Parse(_configurationWrapper.Get("TrainScheduleDataRefreshRateInSeconds"));
            while (!cancellationToken.IsCancellationRequested)
            {
                try
                {
                    await _scheduleProvider.UpdateScheduleAsync(cancellationToken);
                    await Task.Delay(TimeSpan.FromSeconds(dataRefreshIntervalInSeconds));
                }
                catch(Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e);
                }
            }
        }
    }
}
