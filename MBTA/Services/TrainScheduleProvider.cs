﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using MBTA.Interfaces;
using MBTA.Models;

namespace MBTA.Services
{
    public delegate void ScheduleUpdatedHandler(List<TrainScheduleDto> schedule);


    public class TrainScheduleProvider: ITrainScheduleProvider, IDisposable
    {
        public event ScheduleUpdatedHandler OnScheduleChange;
        private ReaderWriterLockSlim _readerWriterLock;
        private IScheduleClient _scheduleClient;
        int lastTimeStamp;
        public TrainScheduleProvider(IScheduleClient scheduleClient)
        {
            _readerWriterLock = new ReaderWriterLockSlim();
            _scheduleClient = scheduleClient;
            lastTimeStamp = int.MinValue;
        }

        private List<TrainScheduleDto> _trainSchedules;

        public List<TrainScheduleDto> TrainSchedules {
            get
            {
                List<TrainScheduleDto> temp;
                _readerWriterLock.TryEnterReadLock(-1);
                temp = _trainSchedules;
                _readerWriterLock.ExitReadLock();
                return temp;
            }
            private set
            {
                _readerWriterLock.TryEnterWriteLock(-1);
                _trainSchedules = value;
                _readerWriterLock.ExitWriteLock();
            }
        }
        public async Task UpdateScheduleAsync(CancellationToken cancellationToken)
        {
            if (!cancellationToken.IsCancellationRequested)
            {
                var data = await _scheduleClient.GetScheduleAsync();
                if( data.Count > 0 && lastTimeStamp != data[0].TimeStamp)
                {
                    TrainSchedules = data;
                    OnScheduleChange?.Invoke(data);
                    lastTimeStamp = data[0].TimeStamp;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _readerWriterLock.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);

        }
        #endregion
    }
}
