﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MBTA.Interfaces;
using MBTA.Models;
using MBTA.Models.Converters;

namespace MBTA.Controllers
{
    [Route("api/board")]
    public class TrainBoardController : Controller
    {
        private ITrainScheduleProvider _scheduleProvider { get; }
        private IConfigurationWrapper _configurationWrapper { get; }
        public TrainBoardController(ITrainScheduleProvider scheduleProvider, IConfigurationWrapper configurationWrapper)
        {
            _scheduleProvider = scheduleProvider;
            _configurationWrapper = configurationWrapper;
        }
        // GET api/values
        [HttpGet]
        public StationBoard Get()
        {
            var stationBoard = new StationBoard
            {
                Items = _scheduleProvider.TrainSchedules.Select(ScheduleDataConverter.ConvertScheduleToBoardItem).ToArray(),
                UpdateChannel = _configurationWrapper.Get("SignalRTrainUrl"),
                UpdateMethod = _configurationWrapper.Get("SignalRReceiveDataMethod")
            };
            return stationBoard;
        }
    }
}
