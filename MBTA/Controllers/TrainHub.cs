﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;

using MBTA.Models;
using MBTA.Models.Converters;
using MBTA.Interfaces;
namespace MBTA
{
    public class TrainHub: Hub
    {
        IHubContext<TrainHub> _context { get; }
        IConfigurationWrapper _configurationWrapper { get; }
        public TrainHub(IHubContext<TrainHub> context, ITrainScheduleProvider scheduleProvider, IConfigurationWrapper configurationWrapper)
        {
            _context = context;
            scheduleProvider.OnScheduleChange += Send;
            _configurationWrapper = configurationWrapper;
        }
        public void Send(List<TrainScheduleDto> items)
        {
            var stationBoardItems = items.Select(ScheduleDataConverter.ConvertScheduleToBoardItem).ToList();
            var clientMethodName = _configurationWrapper.Get("SignalRReceiveDataMethod");
            _context.Clients?.All.InvokeAsync(clientMethodName, stationBoardItems);
        }
    }
}
