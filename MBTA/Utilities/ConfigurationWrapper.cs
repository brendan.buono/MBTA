﻿using System.IO;
using Microsoft.Extensions.Configuration;
using MBTA.Interfaces;

namespace MBTA.Utilities
{
    public class ConfigurationWrapper : IConfigurationWrapper
    {
        private IConfiguration Configuration { get; set; }

        public ConfigurationWrapper()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public string Get(string key)
        {
            return Configuration[key];
        }
    }
}
