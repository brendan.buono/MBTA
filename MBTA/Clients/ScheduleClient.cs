﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MBTA.Interfaces;
using MBTA.Models;
using MBTA.Utilities;
namespace MBTA.Clients
{
    public class ScheduleHttpClient : IScheduleClient
    {
        IConfigurationWrapper _configuration;
        HttpClient _httpClient;
        int _retryAttempts;
        public ScheduleHttpClient(IConfigurationWrapper configuration, int retryAttempts = 3)
        {
            _configuration = configuration;
            _httpClient = new HttpClient();
            _retryAttempts = retryAttempts;
        }

        public async Task<List<TrainScheduleDto>> GetScheduleAsync()
        {
            return await GetScheduleAsync(1);
        }

        private async Task<List<TrainScheduleDto>> GetScheduleAsync(int attemptNumber = 1)
        {
            var response = await _httpClient.GetAsync(_configuration.Get("ScheduleDataUrl"));
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStreamAsync();
                var csvData = new CsvReader(new StreamReader(content));
                csvData.Configuration.HasHeaderRecord = true;
                csvData.Configuration.Delimiter = ",";
                return csvData.GetRecords<TrainScheduleDto>().ToList();
            }
            else
            {
                if (attemptNumber <= _retryAttempts)
                {
                    return await GetScheduleAsync(attemptNumber++);
                }
                else
                {
                    throw new HttpRequestException($"{_retryAttempts} failed");
                }
            }
        }
    }
}
