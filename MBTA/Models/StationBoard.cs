﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MBTA.Models
{
    public class StationBoard
    {
        public StationBoardItem[] Items { get; set; }
        public string UpdateChannel { get; set; }
        public string UpdateMethod { get; set; }
    }
}
