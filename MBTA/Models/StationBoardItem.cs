﻿using System;

namespace MBTA.Models
{
    public class StationBoardItem
    {
        public string Origin { get; set; }
        public DateTime DepartureTime { get; set; }
        public string Destination { get; set; }
        public string TrainNumber { get; set; }
        public string Track { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return $"{Origin} {DepartureTime.ToShortTimeString()} {Destination} {TrainNumber} {Track} {Status}";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var comparer = obj as StationBoardItem;
            if(comparer == null)
            {
                return false;
            }
            return comparer.Origin == Origin &&
                comparer.DepartureTime == DepartureTime &&
                comparer.Destination == Destination &&
                comparer.TrainNumber == TrainNumber &&
                comparer.Track == Track &&
                comparer.Status == Status;

        }
    }
}
