﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MBTA.Models
{
    public class TrainScheduleDto
    {
        public int TimeStamp { get; set; }
        public string Origin { get; set; }
        public string Trip { get; set; }
        public string Destination { get; set; }
        public long ScheduledTime { get; set; }
        public int Lateness { get; set; }
        public int? Track { get; set; }
        public string Status { get; set; }

        public override string ToString()
        {
            return $"{TimeStamp} {Origin} {Trip} {Destination} {ScheduledTime} {Lateness} {Track} {Status}";
        }
    }
}
