﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MBTA.Models.Converters
{
    public static class ScheduleDataConverter
    {
        public static StationBoardItem ConvertScheduleToBoardItem(TrainScheduleDto schedule)
        {
            var result = new StationBoardItem
            {
                Origin = schedule.Origin,
                DepartureTime = new DateTime(1970, 1, 1, 0, 0, 0, 0,DateTimeKind.Utc).AddSeconds(schedule.ScheduledTime).AddSeconds(schedule.Lateness),
                Destination = schedule.Destination,
                Status = schedule.Status,
                TrainNumber = schedule.Trip,
                Track = schedule.Track == null ? "TBD" : schedule.Track.ToString()
            };
            return result;
        }
    }
}
