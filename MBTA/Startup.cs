﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using MBTA.Clients;
using MBTA.Interfaces;
using MBTA.Services;
using MBTA.Utilities;

namespace MBTA
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddMvc();
            services.AddSingleton<ITrainScheduleProvider, TrainScheduleProvider>();
            services.AddSingleton<IHostedService, ScheduleHostedService>();
            services.AddSingleton<IScheduleClient, ScheduleHttpClient>();
            services.AddSingleton<IConfigurationWrapper,ConfigurationWrapper>();
            services.AddSingleton<TrainHub>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSignalR(routes =>
            {
                routes.MapHub<TrainHub>("signalr/trains");
            });
          
            app.UseMvc();
            app.UseFileServer();
        }
    }
}
