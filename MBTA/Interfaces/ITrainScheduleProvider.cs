﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MBTA.Models;
using MBTA.Services;

namespace MBTA.Interfaces
{
    public interface ITrainScheduleProvider
    {
        List<TrainScheduleDto> TrainSchedules { get; }
        Task UpdateScheduleAsync(CancellationToken cancellationToken);
        event ScheduleUpdatedHandler OnScheduleChange;
    }
}
