﻿namespace MBTA.Interfaces
{
    public interface IConfigurationWrapper
    {
        string Get(string key);
    }
}
