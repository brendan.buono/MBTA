﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MBTA.Models;

namespace MBTA.Interfaces
{
    public interface IScheduleClient
    {
        Task<List<TrainScheduleDto>> GetScheduleAsync();
    }
}
