﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Moq;

using MBTA.Interfaces;
using MBTA.Services;
using MBTA.UnitTests.Mocks;
using System.Threading;
using System.Threading.Tasks;

namespace MBTA.UnitTests
{
    [TestFixture]
    class ScheduleHostedServiceTests
    {
        [Test]
        public void TestDataRefreshFromHostedService()
        {
            var mockScheduleProvider = new Mock<ITrainScheduleProvider>();
            var service = new ScheduleHostedService(mockScheduleProvider.Object, new MockConfigurationWrapper());
            var cancellationToken  = new CancellationToken();
            var cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            var executeTask = service.ExecuteAsync(cancellationTokenSource.Token);
            cancellationTokenSource.Cancel();
            Task.WaitAll(executeTask);
            mockScheduleProvider.Verify(x => x.UpdateScheduleAsync(cancellationTokenSource.Token));
        }
    }
}
