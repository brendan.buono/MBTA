using NUnit.Framework;
using MBTA.Models;
using MBTA.Models.Converters;
using System;

namespace MBTA.UnitTests
{
    [TestFixture]
    public class ScheduleConverterTests
    {
        TrainScheduleDto _scheduleDto;
        [SetUp]
        public void Setup()
        {
            _scheduleDto = new TrainScheduleDto()
            {
                Origin = "South Station",
                Destination = "Franklin",
                ScheduledTime = 0,
                Lateness = 120,
                TimeStamp = 1234567890,
                Status = "On Time",
                Track = 1,
                Trip = "123P"

            };
        }

        [Test]
        public void TestStatusConversion()
        {
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.Status, _scheduleDto.Status);
        }

        [Test]
        public void TestDestinationConversion()
        {     
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.Destination, _scheduleDto.Destination);
        }

       [Test]
       public void TestOriginConversion()
        {
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.Origin, _scheduleDto.Origin);
        }
        [Test]
        public void TestTrackWhenSetConversion()
        {
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(int.Parse(result.Track), _scheduleDto.Track);
        }
        [Test]
        public void TestTrackWhenNotSetConversion()
        {
            _scheduleDto.Track = null;
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.Track, "TBD");
        }
        [Test]
        public void TestTrainNumberConversion()
        {
            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.TrainNumber, _scheduleDto.Trip);
        }
        [Test]
        public void TestDepartureTimeConversion()
        {
            var expectedTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(_scheduleDto.ScheduledTime).AddSeconds(_scheduleDto.Lateness);

            var result = ScheduleDataConverter.ConvertScheduleToBoardItem(_scheduleDto);
            Assert.AreEqual(result.DepartureTime, expectedTime);
        }
    }
}