﻿using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Threading.Tasks;

using NUnit.Framework;
using Moq;

using MBTA.Interfaces;
using MBTA.Models;
using MBTA.Models.Converters;
using MBTA.UnitTests.Mocks;

namespace MBTA.UnitTests
{
    [TestFixture]
    public class TrainHubTests
    {
        Mock<ITrainScheduleProvider> _mockTrainScheduleProvider;
        Mock<IHubContext<TrainHub>> _mockHubContext;
        Mock<IHubClients> _mockHubClients;
        MockClientProxy _mockClientProxy;
        List<TrainScheduleDto> _trainSchedules;
        [SetUp]
        public void Setup()
        {
            _mockTrainScheduleProvider = new Mock<ITrainScheduleProvider>();
            _trainSchedules = new List<TrainScheduleDto>();
            _mockTrainScheduleProvider.SetupGet(x => x.TrainSchedules).Returns(_trainSchedules);

            _mockHubContext = new Mock<IHubContext<TrainHub>>();
            _mockHubClients = new Mock<IHubClients>();
            _mockClientProxy = new MockClientProxy();

            _mockHubContext.Setup(x => x.Clients).Returns(_mockHubClients.Object);
            _mockHubClients.Setup(x => x.All).Returns(_mockClientProxy);
        }
        
        [Test]
        public void TestTrainHubClientUpdate()
        {
            var trainSchedule = new TrainScheduleDto()
            {
                Origin = "Hoth",
                Destination = "Endor",
                ScheduledTime = 0,
                Lateness = 0,
                Status = "Entering Lightspeed",
                Track = 1,
                Trip = "Falcon"
            };
            var trainSchedules = new List<TrainScheduleDto> { trainSchedule };

            var trainHub = new TrainHub(_mockHubContext.Object, _mockTrainScheduleProvider.Object, new MockConfigurationWrapper());

            trainHub.Send(trainSchedules);

            Assert.AreEqual(1, _mockClientProxy.InvokeAsyncCallCount);
            Assert.AreEqual(MockConfigurationWrapper.Values.SignalRReceiveDataMethod, _mockClientProxy.InvokeAsyncMethod);
            Assert.AreEqual(trainSchedules.Select(ScheduleDataConverter.ConvertScheduleToBoardItem).ToList(), _mockClientProxy.InvokeAsyncData);
        }
    }
}
