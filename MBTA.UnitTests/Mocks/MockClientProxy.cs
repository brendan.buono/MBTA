﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using System.Threading.Tasks;

using MBTA.Models;

namespace MBTA.UnitTests.Mocks
{
    internal class MockClientProxy : IClientProxy
    {
        public int InvokeAsyncCallCount = 0;
        public string InvokeAsyncMethod = string.Empty;
        public List<StationBoardItem> InvokeAsyncData = new List<StationBoardItem>();
        public Task InvokeAsync(string method, object[] args)
        {
            InvokeAsyncCallCount++;
            InvokeAsyncMethod = method;
            InvokeAsyncData = args[0] as List<StationBoardItem>;
            return Task.CompletedTask;
        }
    }
}
