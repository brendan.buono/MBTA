﻿using MBTA.Interfaces;

namespace MBTA.UnitTests.Mocks
{
    internal class MockConfigurationWrapper : IConfigurationWrapper
    {
        internal class Values
        {
            internal const string ScheduleDataUrl = "http://developer.mbta.com/lib/gtrtfs/Departures.csv";
            internal const string SignalRTrainUrl = "signalr/trains";
            internal const string SignalRReceiveDataMethod = "broadcastMessage";
            internal const string TrainScheduleDataRefreshRateInSeconds = "5";
        }
        public string Get(string key)
        {
            switch (key)
            {
                case nameof(Values.ScheduleDataUrl):
                    return Values.ScheduleDataUrl;
                case nameof(Values.SignalRTrainUrl):
                    return Values.SignalRTrainUrl;
                case nameof(Values.SignalRReceiveDataMethod):
                    return Values.SignalRReceiveDataMethod;
                case nameof(Values.TrainScheduleDataRefreshRateInSeconds):
                    return Values.TrainScheduleDataRefreshRateInSeconds;
                default:
                    return null;

            }
        }
    }
}
