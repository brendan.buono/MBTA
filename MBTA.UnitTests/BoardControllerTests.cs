﻿using Moq;
using System.Collections.Generic;
using NUnit.Framework;
using MBTA.Controllers;
using MBTA.Interfaces;
using MBTA.Models;
using MBTA.Models.Converters;
using MBTA.UnitTests.Mocks;

namespace MBTA.UnitTests
{
    [TestFixture]
    public class BoardControllerTests
    {
        Mock<ITrainScheduleProvider> _mockTrainScheduleProvider;
        List<TrainScheduleDto> _trainSchedules;
        [SetUp]
        public void Setup()
        {
            _mockTrainScheduleProvider = new Mock<ITrainScheduleProvider>();
            _trainSchedules = new List<TrainScheduleDto>();
            _mockTrainScheduleProvider.SetupGet(x => x.TrainSchedules).Returns(_trainSchedules);
        }
        
        [Test]
        public void TestStationBoardGet()
        {
            var trainSchedule = new TrainScheduleDto()
            {
                Origin = "Hoth",
                Destination = "Endor",
                ScheduledTime = 0,
                Lateness = 0,
                Status = "Entering Lightspeed",
                Track = 1,
                Trip = "Falcon"
            };
            _trainSchedules.Add(trainSchedule);
            var controller = new TrainBoardController(_mockTrainScheduleProvider.Object, new MockConfigurationWrapper());
            var board = controller.Get();
            Assert.AreEqual(MockConfigurationWrapper.Values.SignalRTrainUrl, board.UpdateChannel);
            Assert.AreEqual(MockConfigurationWrapper.Values.SignalRReceiveDataMethod, board.UpdateMethod);
            Assert.AreEqual(1, board.Items.Length);
            Assert.AreEqual(ScheduleDataConverter.ConvertScheduleToBoardItem(trainSchedule), board.Items[0]);
        }
    }
}
